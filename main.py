# Run in Python 3.8
import yaml

class Field:
    def __init__(self, name, value, type=None):
        self.name = name
        self.value = valueToString(value)
        if type is None:
            self.type = guessType(value)
        else:
            self.type = type

    def __str__(self):
        return "{}:{} = {}".format(self.name, self.type, self.value)

    def __repr__(self):
        return self.__str__()

    def toVar(self):
        return '"{}": {}'.format(self.name, self.value)

    def toQueryParam(self):
        return '${}:{}'.format(self.name, self.type)

    def toNamedParam(self):
        return '{}:${}'.format(self.name, self.name)


# Default to ! since otherwise makes GraphQL angry
def guessType(value, required=True):
    if isinstance(value, str):
        return "String" + ("!" if required else "")
    if isinstance(value, bool):
        return "Boolean" + ("!" if required else "")
    # Long is more common than Int
    if isinstance(value, int):
        return "Long" + ("!" if required else "")
    if isinstance(value, float):
        return "Float" + ("!" if required else "")
    if isinstance(value, list):
        return "[{}]".format(guessType(value[0], False)) + ("!" if required else "")
    print('Did not recognize type {}. If it is null, it\'s type must be defined.'.format(type(value)))


def valueToString(value):
    if value is None:
        return 'null'
    elif isinstance(value, str):
        return '"{}"'.format(value)
    elif isinstance(value, bool):
        return str(value).lower()
    elif isinstance(value, list):
        valueList = '['
        for item in value:
            valueList += valueToString(item) + ", "
        # Remove trailing comma, finish list
        valueList = valueList[:-2] + "]"
        return valueList
    else:
        return str(value)


def printOutputList(output, surroundWithBrackets=True):
    if output is None:
        return
    if surroundWithBrackets:
        print("{ ", end="")
    for field in output:
        if(isinstance(field, str)):
            print(field, end=' ')
        else:
            # If it's not a string, it's a dict with 1 element (another list)
            # Handle it recursively
            fieldName = list(field)[0]
            subList = field[fieldName]
            print(fieldName, end=' { ')
            printOutputList(subList, False)
            print('} ', end='')
    if surroundWithBrackets:
        print(" } ", end="")


# Load the file
with open("QueryFrame.yaml", "r") as stream:
    try:
        data = yaml.safe_load(stream)
    except yaml.YAMLError as exc:
        print(exc)

# Gather the data
queryType = data['type']
queryName = data['name']
fields = []
if data['fields'] is not None:
    for fieldName, value in data['fields'].items():
        if isinstance(value, dict):
            # fieldName:
            #  fieldType: fieldValue
            fieldType = list(value)[0]
            fieldValue = value[fieldType]
            field = Field(fieldName, fieldValue, fieldType)
        else:
            # fieldName: fieldValue
            field = Field(fieldName, value)
        fields.append(field)
output = data['output']

# Print the query
print('{')
if fields:
    print('\t"variables": {')
    for i in range(len(fields)):
        includeComma = i < len(fields) - 1
        print('\t\t' + fields[i].toVar(), end=',\n' if includeComma else '\n')
    print('\t},')
print('\t"query": "{} '.format(queryType), end='')
if fields:
    print('(', end='')
    for i in range(len(fields)):
        includeComma = i < len(fields) - 1
        print(fields[i].toQueryParam(), end=', ' if includeComma else ' ')
    print(') ', end='')
print('{{ {} '.format(queryName), end='')
if fields:
    print('(', end='')
    for i in range(len(fields)):
        includeComma = i < len(fields) - 1
        print(fields[i].toNamedParam(), end=', ' if includeComma else ' ')
    print(') ', end='')
printOutputList(output)
print('}"')
print("}")